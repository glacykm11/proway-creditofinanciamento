/* Alert de pedido realizado e recarregar a página */
class Recarregar{
    constructor(){
        this.botao = document.getElementById('botaopedido');
        this.createListener();
    }
    createListener(){
        document.getElementById('botaopedido').addEventListener('click', () => this.realizarPedido());
    }
    realizarPedido(){
        alert("Pedido realizado!");
        document.location.reload(true);
    }
}

let recarregar = new Recarregar();