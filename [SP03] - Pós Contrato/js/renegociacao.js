class Pos {

    constructor() {
      this.container = document.getElementById('container').textContent; //puxou valor 75.000 do html

      let back = document.getElementById('comeBack');
      back.addEventListener('click', this.message);

      let qtdParcelaElement = document.getElementById('qtdParcela') // pegou o elemento input
      qtdParcelaElement.addEventListener('blur', this.calcParcela)

      this.simbolo = document.getElementById('action');
      this.createGoogleCharts()
    }

  
    calcParcela(){
      let saldoDevedor = document.getElementById('saldoDevedor').textContent
      let qtdParcela = document.getElementById('qtdParcela').value
      let valorParcela = (parseFloat(saldoDevedor)/qtdParcela)*1000
      let juros = valorParcela*0.10
      let valorParcelaComJuros = valorParcela+juros
      let valorParcelaComJurosFormatado = valorParcelaComJuros.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})

      document.getElementById('valorParcela').value = valorParcelaComJurosFormatado
    }
  
    message() {
      alert('volta pra página inicial');
    }
  
    titleClick() {
      alert('encaminha menu de crédito');
    }
  
    buttonVisualizar() {
      alert('saldo mudou de visibilidade');
    }
  
    buttonRenegocie(event) {
      event.preventDefault();
      alert('Renegocie seu crédito');
    }
    createListener() {
      simbolo.addEventListener('click', function () {
        let container = document.getElementById('container');
      });
  
    }
    createGoogleCharts(params) {
      google.charts.load('current', { 'packages': ['corechart'] }); //refazer posteriormente
      google.charts.setOnLoadCallback(this.drawChart);
    }
  
    drawChart() {
      let data = google.visualization.arrayToDataTable([
        ['Year', 'Crédito', 'Debito'],
        ['2004', 1000, 400],
        ['2005', 1170, 460],
        ['2006', 660, 1120],
        ['2007', 1030, 540]
      ]);
  
      let options = {
        title: 'variação do crédito anual',
        curveType: 'function',
        legend: { position: 'bottom' }
      };
  
      let chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
  
      chart.draw(data, options);
    }
  }
  new Pos()
  
  
  
  
  
        // function olho (){
  //  document.getElementById('container').src="/img/EyeSlash.svg";
  //   document.getElementById('container').src="/eye-regular.svg";
  // }
  // document.getElementById('container').addEventListener("click",olho())
  