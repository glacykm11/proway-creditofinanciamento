class SimuladorDeFinancimento{
    constructor(){
        //radio button da parte com entrada e sem entrada
        this.radios = document.getElementsByClassName("form-check-input");
        //variaveis para simulação financimento
        this.valor_prest;
        this.valor_financ;
        this.entrada;
        this.comp_renda;
        this.parcela;
        this.juros_mes;
        this.juros_pagar;
        this.total_finac;

        this.inputs = document.getElementsByClassName("form-control");
        this.parcelas = document.getElementsByClassName("parcelas");

        this.valores = [''];
        this.getParcela = '';       
        
        this.createListener();
    }

    createListener(){
        // for para pegar o radio button
        for (let i = 0; i < this.radios.length; i++){
            this.radios[i].addEventListener("change", (e) => {
                this.limparOption(e.target);    
            })
        }
        // click do botao simular
        document.getElementById('botao').onclick = () =>{
            //pega os valores dos inputs e guarda no array
            for (let a = 0; this.inputs.length > a; a++){
                this.valores[a] = this.inputs[a].value.replace(/[^0-9,]*/g, '').replace(',', '.');
            }
            //for para pegar a parcela selecionada
            for (let p = 0; this.parcelas.length > p; p++){
                this.getParcela = this.parcelas[p].options[this.parcelas[p].selectedIndex].value
            }
            this.calculoFinancimento();
        }
    }


    // se radio sem entrada for clicado ele limpa o valor que tiver em com entrada
    limparOption = (e) => {
        if (e.value == "option2"){
            let valor = document.getElementById("valor");
            valor.value = "";
        }
    }
    

    //verifica comprometimento da renda
    compRenda = (comp_da_renda) => {
        if (this.valores[0] < comp_da_renda){
            comp_da_renda = "Financimento não viável, passa de 100% da renda.";
        } else {
            comp_da_renda = "Financimento de acordo com a renda.";
        }
        this.comp_renda = comp_da_renda;
        return this.comp_renda;
    }

    //função para simular o financiamento
    calculoFinancimento = () => {
        
        if (this.valores[2] != ''){
            this.entrada = this.valores[2];
            this.valores[1] = this.valores[1] - this.entrada;
        } else{
            this.entrada = 0;
        }

        this.valor_prest = this.valores[1] / this.getParcela; 
        this.valor_financ = this.valores[1];
        this.juros_mes = this.mudarJuros(this.juros_mes);
        this.valor_prest = (this.valor_prest * this.juros_mes) + this.valor_prest;
        this.parcela = this.getParcela;
        this.valor_financ = parseFloat(this.valor_financ);
        this.juros_pagar = (this.valor_prest * this.parcela) - this.valor_financ;
        this.juros_mes = (this.valor_prest / this.valor_financ);
        
        this.total_finac = this.valor_financ + this.juros_pagar;
        //chama função para ver se financimento é viável de acordo com a renda
        this.compRenda(this.valor_prest);
  
        document.getElementById("valor_prest").innerHTML = parseFloat(this.valor_prest).toFixed(2).replace('.',',')
        .replace(/([0-9]*)([0-9]{3},*)/, '$1.$2');
        document.getElementById("valor_financ").innerHTML = parseFloat(this.valor_financ).toFixed(2).replace('.',',')
        .replace(/([0-9]*)([0-9]{3},*)/, '$1.$2');
        document.getElementById("entrada").innerHTML = parseFloat(this.entrada).toFixed(2).replace('.',',')
        .replace(/([0-9]*)([0-9]{3},*)/, '$1.$2');
        document.getElementById("comp_renda").innerHTML = this.comp_renda;
        document.getElementById("parcela").innerHTML = this.parcela;
        document.getElementById("juros_mes").innerHTML = parseFloat(this.juros_mes*100).toFixed(2).replace('.',',') + "%";
        document.getElementById("juros_pagar").innerHTML = parseFloat(this.juros_pagar).toFixed(2).replace('.',',')
        .replace(/([0-9]*)([0-9]{3},*)/, '$1.$2');
        document.getElementById("total_finac").innerHTML = parseFloat(this.total_finac).toFixed(2).replace('.',',')
        .replace(/([0-9]*)([0-9]{3},*)/, '$1.$2');

    }
    // mudar juros conforme option
    mudarJuros = (muda_jur) => {
        muda_jur = this.getParcela;
        if(muda_jur == "6")
            muda_jur = 0.25;
        if(muda_jur == "12")
            muda_jur = 0.35;    
        if(muda_jur == "24")
            muda_jur = 0.45;
        if(muda_jur == "36")
            muda_jur = 0.50;
        if(muda_jur == "48")
            muda_jur = 0.55;
        if(muda_jur == "60")
            muda_jur = 0.65;
        if(muda_jur == "72")
            muda_jur = 0.75;
        if(muda_jur == "88")
            muda_jur = 0.85;
        if(muda_jur == "96")
            muda_jur = 0.95;    
        return muda_jur;
    }

}

let simuladorDeFinancimento = new SimuladorDeFinancimento();


/*
function financiamentoCarros(){
    let opcao = document.getElementById('opcao').value;
    switch (opcao){
        case 'credito':
            document.getElementById('botao').href = "../[SP07] - Credito Pessoal/index.html"
        break;

        case 'veiculos':
            document.getElementById('botao').href = "../[SP05] Financiamento de Carros/Financiamento de carros.html";
        break;

        case 'imoveis':
            document.getElementById('botao').href = "../[SP04] - Crédito Imobiliário/Credito Imobiliario.html";
        break;

        case 'celular':
            document.getElementById('botao').href = "../[SP06]-Financiamento de Smartphone/financSmartPhone.html";
        break;
    }    
}
document.getElementById('opcao').addEventListener("change", financiamentoCarros);
replace(/\D+/g, ''); remover qualquer coisa que não seja numero



*/