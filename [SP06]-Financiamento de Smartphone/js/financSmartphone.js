class trocarSmartphone{
    constructor(){
        this.celular = document.getElementById("smartphones")
        this.botao = document.getElementById('botaoRealizarPedido');
        this.createListener()
    }

    createListener(){
        document.getElementById("smartphones").addEventListener("change", () => this.habilitarParcelas())
        document.getElementById("smartphones").addEventListener("change", () => this.trocarImagem())
        document.getElementById("smartphones").addEventListener("change", () => this.mostrarParcelas())
        document.getElementById('botaoRealizarPedido').addEventListener('click', () => this.recarregarPagina());
    }
    //Troca imagem do smartphone de acordo com a opção selecionada
    trocarImagem(){
        if (this.celular.value == "iphone12"){
            document.getElementById("fotoSmartphone").src= "/[SP06]-Financiamento de Smartphone/img/iphone12.png"
        }else if(this.celular.value == "iphone11"){
                document.getElementById("fotoSmartphone").src= "/[SP06]-Financiamento de Smartphone/img/iphone11.png"
        }
        else if(this.celular.value == "iphone10"){
            document.getElementById("fotoSmartphone").src= "/[SP06]-Financiamento de Smartphone/img/iphone10.jpg"
        }
        else if(this.celular.value == "iphone8"){
            document.getElementById("fotoSmartphone").src= "/[SP06]-Financiamento de Smartphone/img/iphone8.png"
        }else if(this.celular.value == "samsungS20"){
            document.getElementById("fotoSmartphone").src= "/[SP06]-Financiamento de Smartphone/img/samsungS20.png"
        }
        else if(this.celular.value == "samsungfold"){
            document.getElementById("fotoSmartphone").src= "/[SP06]-Financiamento de Smartphone/img/samsungfold.png"
        }
    }
    //Troca as parcelas do smartphone de acordo com a opção selecionada
    mostrarParcelas(){
        if(this.celular.value == "iphone12"){
            document.getElementById("parcelas").innerHTML = 
            '<option value="parcela1">1x R$ 6.999,00 à vista</option> <option value="parcela2">2x R$ 3.299,50 sem juros</option><option value="parcela3">3x R$ 2.199,67 sem juros</option><option value="parcela4">4x R$ 1.649,75 sem juros</option><option value="parcela5">5x R$ 1.319,80 sem juros</option>'
        }else if(this.celular.value == "iphone11"){
            document.getElementById("parcelas").innerHTML =    
            '<option value="parcela1">1x R$ 4.599,00 à vista</option> <option value="parcela2">2x R$ 2.999,50 sem juros</option><option value="parcela3">3x R$ 1.666,33 sem juros</option><option value="parcela4">4x R$ 1.249,75 sem juros</option><option value="parcela5">5x R$ 999,80 sem juros</option>'
        }else if(this.celular.value == "iphone10"){
            document.getElementById("parcelas").innerHTML= 
            '<option value="parcela1">1x R$ 3.999,00 à vista</option> <option value="parcela2">2x R$ 1.799,50 sem juros</option><option value="parcela3">3x R$ 1.199,67 sem juros</option><option value="parcela4">4x R$ 899,75 sem juros</option><option value="parcela5">5x R$ 719,80 sem juros</option>'
        }else if(this.celular.value == "iphone8"){
            document.getElementById("parcelas").innerHTML = 
            '<option value="parcela1">1x R$ 3.499,99 à vista</option> <option value="parcela2">2x R$ 1.750,00 sem juros</option><option value="parcela3">3x R$ 1.166,66 sem juros</option><option value="parcela4">4x R$ 875,00 sem juros</option><option value="parcela5">5x R$ 700,00 sem juros</option>' 
        }else if(this.celular.value == "samsungS20"){
            document.getElementById("parcelas").innerHTML = 
            '<option value="parcela1">1x R$ 5.114,91 à vista</option> <option value="parcela2">2x R$ 2.749,95 sem juros</option><option value="parcela3">3x R$ 1.833,30 sem juros</option><option value="parcela4">4x R$ 1.374,98 sem juros</option><option value="parcela5">5x R$ 1.099,98 sem juros</option>' 
        }
        else if(this.celular.value == "samsungfold"){
            document.getElementById("parcelas").innerHTML = 
            '<option value="parcela1">1x R$ 11.699,10 à vista</option> <option value="parcela2">2x R$ 6.499,50 sem juros</option><option value="parcela3">3x R$ 4.333,00 sem juros</option><option value="parcela4">4x R$ 3.249,75 sem juros</option><option value="parcela5">5x R$ 2.599,80 sem juros</option>' 
        }
    }
    //Habilita o select das parcelas
    habilitarParcelas(){
        document.getElementById("parcelas").disabled = false
    }
    
    recarregarPagina(){
        alert("Pedido realizado!");
        document.location.reload(true);
    }  
}

let trocaSmartphone = new trocarSmartphone();